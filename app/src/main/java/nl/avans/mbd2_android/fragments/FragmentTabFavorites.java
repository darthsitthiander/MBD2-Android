package nl.avans.mbd2_android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.List;

import nl.avans.mbd2_android.R;
import nl.avans.mbd2_android.activities.DetailsActivity;
import nl.avans.mbd2_android.activities.MainActivity;
import nl.avans.mbd2_android.adapter.CustomListAdapter;
import nl.avans.mbd2_android.adapter.ListAdapterNowOpen;
import nl.avans.mbd2_android.models.AsyncListViewLoader;
import nl.avans.mbd2_android.models.AsyncListViewLoaderMultiple;
import nl.avans.mbd2_android.models.AsyncResponse;
import nl.avans.mbd2_android.models.SharedPreference;
import nl.avans.mbd2_android.models.Venue;

public class FragmentTabFavorites extends Fragment {

    TabFavoritesListener activityCommander;

    private ListAdapterNowOpen itemArrayAdapter;
    private ListView listView;
    private static ViewGroup container;
    private LayoutInflater inflater;
    private View viewNoData;

    public static FragmentTabFavorites newInstance(ListView listView) {
        FragmentTabFavorites f = new FragmentTabFavorites();

        f.listView = listView;

        return(f);
    }

    public FragmentTabFavorites() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            activityCommander = (TabFavoritesListener) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        View view;

        // check favorites
        if(activityCommander.getFavorites() != null && activityCommander.getFavorites().size() > 0) {
            view = inflater.inflate(R.layout.layout_fragment, container, false);
            final ListView lv = (ListView) view.findViewById(R.id.fragment_list_view);
            lv.setAdapter(this.listView.getAdapter());
            lv.setOnItemClickListener(this.listView.getOnItemClickListener());
            SearchView sv = (SearchView) view.findViewById(R.id.fragment_filter);
            sv.setFocusable(false);
            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {

                public boolean onQueryTextChange(String newText) {
                    // this is your adapter that will be filtered
                    ((CustomListAdapter) lv.getAdapter()).getFilter().filter(newText);
                    return true;
                }

                public boolean onQueryTextSubmit(String query) {
                    // this is your adapter that will be filtered
                    ((CustomListAdapter) lv.getAdapter()).getFilter().filter(query);
                    return true;
                }
            };
            sv.setOnQueryTextListener(queryTextListener);
            ((CustomListAdapter) lv.getAdapter()).notifyDataSetChanged();
        } else {
            view = inflater.inflate(R.layout.fragment_tab_favorites, container, false);
        }
        return view;
    }

    public void refresh() {
        getFragmentManager()
                .beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }

    public interface TabFavoritesListener{
        public List<String> getFavorites();
    }
}