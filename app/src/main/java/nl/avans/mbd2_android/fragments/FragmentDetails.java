package nl.avans.mbd2_android.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

import nl.avans.mbd2_android.R;
import nl.avans.mbd2_android.models.DownloadImageTask;
import nl.avans.mbd2_android.models.Venue;

/**
 * Created by Sander on 8-4-2015.
 */
public class FragmentDetails extends Fragment {

    // LayoutInflator puts the Fragment on the screen
    // ViewGroup is the view you want to attach the Fragment to
    // Bundle stores key value pairs so that data can be saved
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }

        View view =  inflater.inflate(R.layout.listview_item_detail, container, false);

        Venue lI = (Venue) getActivity().getIntent().getSerializableExtra("item");

        if (lI == null) {
            Random r = new Random();
            int randomID = (r.nextInt(80) + 65);
            lI = new Venue(String.valueOf(randomID));
            lI.setBody("Some random body text for item # "+getShownIndex());
            lI.setTitle("Item Header # " + (getShownIndex()-1) );
        }
        if(lI.getTitle() != null) {
            TextView tvTitle = (TextView) view.findViewById(R.id.detail_itemTitle);
            tvTitle.setText(lI.getTitle());
        }
        if(lI.getBody() != null) {
            TextView tvBody = (TextView) view.findViewById(R.id.detail_itemBody);
            tvBody.setText(lI.getBody());
        }
        if(lI.getTagline() != null && !lI.getTagline().equals("null")){
            TextView tvTagline = (TextView) view.findViewById(R.id.detail_itemTagline);
            tvTagline.setText(lI.getTagline());
        }
        if(lI.getRating() != 0.0f) {
            TextView tvRating = (TextView) view.findViewById(R.id.detail_itemRating);
            tvRating.setText(lI.getRating().toString());
        }
        if(lI.getPhoneNumber() != null) {
            TextView tvPhone = (TextView) view.findViewById(R.id.detail_itemPhone);
            final String phoneNumber = lI.getPhoneNumber();
            tvPhone.setText(phoneNumber);
            tvPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + phoneNumber));
                    startActivity(callIntent);
                }
            });
        }
        if(lI.getWebsiteURL() != null) {
            TextView tvWebsite = (TextView) view.findViewById(R.id.detail_itemWebsite);
            final String webURL = lI.getWebsiteURL();
            tvWebsite.setText(webURL);
            tvWebsite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(webURL));
                    startActivity(browserIntent);
                }
            });
        }
        if(lI.getAdress() != null) {
            TextView tvAdress = (TextView) view.findViewById(R.id.detail_itemAdres);
            tvAdress.setText(lI.getAdress());
        }
        String url = lI.getImageURLCropped();
        if (url != null) {
            ImageView iV = (ImageView) view.findViewById(R.id.detail_itemImage);
            iV.setTag(url);
            new DownloadImageTask(iV).execute(url);
        }

        return view;
    }

    // Create a DetailsFragment that contains the hero data for the correct index
    public static FragmentDetails newInstance(int index) {
        FragmentDetails f = new FragmentDetails();

        // Bundles are used to pass data using a key "index" and a value
        Bundle args = new Bundle();
        args.putInt("index", index);

        // Assign key value to the fragment
        f.setArguments(args);

        return f;
    }

    public int getShownIndex() {

        // Returns the index assigned
        int val = -1;
        if(getArguments() != null){
            val = getArguments().getInt("index", 0);
        }
        return val ;
    }


}