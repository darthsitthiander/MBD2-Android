package nl.avans.mbd2_android.fragments;

        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.os.Bundle;
        import android.app.Fragment;
        import android.preference.PreferenceManager;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.ListView;
        import android.widget.SearchView;

        import java.util.Map;

        import nl.avans.mbd2_android.R;
        import nl.avans.mbd2_android.activities.DetailsActivity;
        import nl.avans.mbd2_android.adapter.CustomListAdapter;
        import nl.avans.mbd2_android.adapter.ListAdapterNowOpen;
        import nl.avans.mbd2_android.models.AsyncListViewLoaderMultiple;
        import nl.avans.mbd2_android.models.AsyncResponse;
        import nl.avans.mbd2_android.models.Venue;

public class FragmentTabNowOpen extends Fragment implements AsyncResponse {

    private ListAdapterNowOpen itemArrayAdapter;
    String jsonFeed;
    int mCurCheckPosition;
    private SharedPreferences prefs;
    private ListView listView;

    public static FragmentTabNowOpen newInstance(ListView listView) {
        FragmentTabNowOpen f = new FragmentTabNowOpen();

        f.listView = listView;

        return(f);
    }

    public FragmentTabNowOpen() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        View view;
        if(this.listView == null){
            view = inflater.inflate(R.layout.fragment_tab_favorites, container, false);
        } else {
            view = inflater.inflate(R.layout.layout_fragment, container, false);
            final ListView lv = (ListView) view.findViewById(R.id.fragment_list_view);
            lv.setAdapter(this.listView.getAdapter());
            lv.setOnItemClickListener(this.listView.getOnItemClickListener());
            SearchView sv = (SearchView) view.findViewById(R.id.fragment_filter);
            sv.setFocusable(false);
            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {

                public boolean onQueryTextChange(String newText) {
                    // this is your adapter that will be filtered
                    ((CustomListAdapter) lv.getAdapter()).getFilter().filter(newText);
                    return true;
                }

                public boolean onQueryTextSubmit(String query) {
                    // this is your adapter that will be filtered
                    ((CustomListAdapter) lv.getAdapter()).getFilter().filter(query);
                    return true;
                }
            };
            sv.setOnQueryTextListener(queryTextListener);
            ((CustomListAdapter) lv.getAdapter()).notifyDataSetChanged();
        }
        return view;
    }

    public void processFinish(String output)
    {
        jsonFeed = output;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // If the screen is rotated onSaveInstanceState() below will store the // hero most recently selected. Get the value attached to curChoice and // store it in mCurCheckPosition
        if (savedInstanceState != null) {
            // Restore last state for checked position.
            mCurCheckPosition = savedInstanceState.getInt("mCurCheckPosition", 0);
        } else {
            prefs = getActivity().getSharedPreferences("nl.avans.mbd2_android", Context.MODE_PRIVATE);
            mCurCheckPosition = prefs.getInt("mCurCheckPosition", 0);
        }
    }

    // Called every time the screen orientation changes or Android kills an Activity
    // to conserve resources
    // We save the last item selected in the list here and attach it to the key curChoice
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mCurCheckPosition", mCurCheckPosition);
    }
}