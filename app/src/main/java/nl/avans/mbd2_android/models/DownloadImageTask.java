package nl.avans.mbd2_android.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Sander on 8-4-2015.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    ImageView imageView = null;

    public DownloadImageTask(ImageView imageView) {

        this.imageView = imageView;
    }

    protected Bitmap doInBackground(String... params) {
        // TODO Auto-generated method stub

        String urlStr = params[0];

        // prevent adapter recycling from loading wrong images
        if (imageView.getTag() != null && imageView.getTag().equals(urlStr)) {

            Bitmap img = null;

            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(urlStr);
            HttpResponse response;
            try {
                response = (HttpResponse) client.execute(request);
                HttpEntity entity = response.getEntity();
                BufferedHttpEntity bufferedEntity = new BufferedHttpEntity(entity);
                InputStream inputStream = bufferedEntity.getContent();
                img = BitmapFactory.decodeStream(inputStream);
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return img;
        } else {
            return null;
        }
    }

    protected void onPostExecute(Bitmap result) {
        imageView.setImageBitmap(result);
    }
}