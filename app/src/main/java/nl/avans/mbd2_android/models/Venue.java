package nl.avans.mbd2_android.models;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Sander on 7-4-2015.
 */
public class Venue implements Serializable {
    private String id;
    private String venueID;
    private String title;
    private String body;
    private String imageURLOriginal;
    private String imageURLCropped;
    private String tagline;
    private Float rating;
    private String phoneNumber;
    private String adress;
    private String websiteURL;

    public Venue(String venueID){
        this.venueID = venueID;
    }
    
    public Venue(String venueID, String title, String body){
        this.venueID = venueID;
        this.setTitle(title);
        this.setBody(body);
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public void setBody(String body){
        this.body = body;
    }

    public String getBody(){
        return this.body;
    }

    public String getId(){
        return this.id;
    }

    public String getImageURLCropped() {
        return imageURLCropped;
    }

    public void setImageURLCropped(String imageURLCropped) {
        this.imageURLCropped = imageURLCropped;
    }

    public String getImageURLOriginal() {
        return imageURLOriginal;
    }

    public void setImageURLOriginal(String imageURLOriginal) {
        this.imageURLOriginal = imageURLOriginal;
    }

    public String getVenueID() {
        return venueID;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = (float) rating / 10;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String result) {
        JSONObject jsonObject = null;
        String adress = "";
        try {
            jsonObject = new JSONObject(result);
            adress += jsonObject.optString("street") + "\n";
            adress += jsonObject.optString("zipcode") + " " + jsonObject.optString("city") + "\n";
            adress += jsonObject.optString("region");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.adress = adress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
