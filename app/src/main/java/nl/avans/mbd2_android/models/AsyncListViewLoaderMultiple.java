package nl.avans.mbd2_android.models;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import nl.avans.mbd2_android.adapter.CustomListAdapter;
import nl.avans.mbd2_android.fragments.FragmentTabFavorites;

/**
 * Created by Sander on 7-4-2015.
 */

public class AsyncListViewLoaderMultiple extends AsyncTask<String, Void, ArrayList<Venue>>{

    private ProgressDialog dialog;

    public AsyncResponse delegate = null;
    private Activity context;
    private ListView listView;
    private FragmentTabFavorites fragmentTabFavorites;

    public AsyncListViewLoaderMultiple(Activity context, AsyncResponse delegate, ListView list) {
        this.context = context;
        this.delegate = delegate;
        this.listView = list;
    }

    public AsyncListViewLoaderMultiple(Activity context, AsyncResponse delegate, ListView list, FragmentTabFavorites fragment) {
        this.context = context;
        this.delegate = delegate;
        this.listView = list;
        this.fragmentTabFavorites = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dialog = new ProgressDialog(context);
        dialog.setMessage("Bezig met laden...");
        dialog.show();
    }

    @Override
    protected ArrayList<Venue> doInBackground(String... uri) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            response = httpclient.execute(new HttpGet(uri[0]));
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
        } catch (IOException e) {
            //TODO Handle problems..
        }
        ArrayList<Venue> items = getListItems(responseString);
        return items;
    }

    @Override
    protected void onPostExecute(ArrayList<Venue> result) {
        super.onPostExecute(result);

        CustomListAdapter clAdapter = (CustomListAdapter) listView.getAdapter();
        clAdapter.setListData(result);

        dialog.dismiss();
    }

    private ArrayList<Venue> getListItems(String result) {
        ArrayList<Venue> items = new ArrayList<Venue>();
        JSONObject json = null;
        try {
            json = new JSONObject(result);
            JSONArray jsonArray = json.getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Venue item = new Venue(jsonObject.optString("id"));
                item.setTitle(jsonObject.optString("name"));
                item.setBody(jsonObject.optString("category"));
                item.setTagline(jsonObject.optString("tagline"));
                item.setRating(jsonObject.optInt("rating"));
                item.setPhoneNumber(jsonObject.optString("telephone"));
                item.setWebsiteURL(jsonObject.optString("website_url"));
                item.setAdress(jsonObject.optString("address"));
                if(jsonObject.getJSONObject("images") != null){
                    JSONObject imagesObject = jsonObject.getJSONObject("images");
                    JSONArray imagesArrayOriginal = imagesObject.getJSONArray("cropped");
                    JSONArray imagesArrayCropped = imagesObject.getJSONArray("cropped");
                    if(imagesArrayCropped.length() > 0) {
                        item.setImageURLCropped(imagesArrayCropped.getString(0));
                        item.setImageURLOriginal(imagesArrayOriginal.getString(0));
                    }
                }
                items.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //for(ListItemOld file:items) Log.d("item", "list: " + file.getImageURLCropped());
        return items;
    }
}