package nl.avans.mbd2_android.models;

/**
 * Created by Sander on 7-4-2015.
 */
public interface AsyncResponse
{
    void processFinish(String output);
}
