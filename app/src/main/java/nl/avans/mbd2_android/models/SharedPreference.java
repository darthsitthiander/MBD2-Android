package nl.avans.mbd2_android.models;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by Sander on 9-4-2015.
 */
public class SharedPreference {

    public static final String PREFS_NAME = "venue_favorites";
    public static final String FAVORITES = "venue_favorite";

    public SharedPreference() {
        super();
    }

    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<String> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Set<String> set = new HashSet<String>();
        set.addAll(favorites);
        editor.putStringSet(FAVORITES, set);


        editor.commit();
    }

    public void addFavorite(Context context, String venueID) {
        List<String> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<String>();
        favorites.add(venueID);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, String venueID) {
        ArrayList<String> favorites = getFavorites(context);
        if (favorites != null) {
            favorites.remove(venueID);
            saveFavorites(context, favorites);
        }
    }

    public ArrayList<String> getFavorites(Context context) {
        SharedPreferences settings;
        List<String> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            Set<String> set = settings.getStringSet(FAVORITES, null);
            favorites = new ArrayList<String>(set);
        } else
            return null;

        return (ArrayList<String>) favorites;
    }

    public boolean inFavorites(Context context, String venueID){
        ArrayList<String> favorites = getFavorites(context);
        if(favorites != null && favorites.contains(venueID)){
            return true;
        }
        return false;
    }
}