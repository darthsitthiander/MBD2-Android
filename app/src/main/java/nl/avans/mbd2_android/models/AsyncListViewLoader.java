package nl.avans.mbd2_android.models;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nl.avans.mbd2_android.adapter.CustomListAdapter;
import nl.avans.mbd2_android.fragments.FragmentTabFavorites;

/**
 * Created by Sander on 9-4-2015.
 */
public class AsyncListViewLoader extends AsyncTask<List<String>, Void, ArrayList<Venue>> {

        private ProgressDialog dialog;

        public AsyncResponse delegate = null;
        private Activity context;
        private ListView listView;

        public AsyncListViewLoader(Activity context, AsyncResponse delegate, ListView list) {
            this.context = context;
            this.delegate = delegate;
            this.listView = list;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(context);
            dialog.setMessage("Bezig met laden...");
            dialog.show();
        }

        @Override
        protected ArrayList<Venue> doInBackground(List<String>... params) {
            JSONArray jsonArray = new JSONArray();

            ArrayList<Venue> venues = new ArrayList<Venue>();
            for(String venueID : params[0]) {
                String responseVenue = getResponseVenue(venueID);

                Venue venue = getVenue(responseVenue);
                venues.add(venue);
            }
            return venues;
        }

    private String getResponseVenue(String venueID) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = "";
        try {
            response = httpclient.execute(new HttpGet("https://api.eet.nu/venues/" + venueID));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    responseString = out.toString();
                    out.close();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(ArrayList<Venue> result) {
            super.onPostExecute(result);

            CustomListAdapter clAdapter = (CustomListAdapter) listView.getAdapter();
            clAdapter.setListData(result);
            clAdapter.notifyDataSetChanged();

            dialog.dismiss();
        }

        private Venue getVenue(String result) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(result);
                Venue item = new Venue(jsonObject.optString("id"));
                item.setTitle(jsonObject.optString("name"));
                item.setBody(jsonObject.optString("category"));
                item.setTagline(jsonObject.optString("tagline"));
                item.setRating(jsonObject.optInt("rating"));
                item.setPhoneNumber(jsonObject.optString("telephone"));
                item.setWebsiteURL(jsonObject.optString("website_url"));
                item.setAdress(jsonObject.optString("address"));
                if(jsonObject.getJSONObject("images") != null){
                    JSONObject imagesObject = jsonObject.getJSONObject("images");
                    JSONArray imagesArrayOriginal = imagesObject.getJSONArray("cropped");
                    JSONArray imagesArrayCropped = imagesObject.getJSONArray("cropped");
                    if(imagesArrayCropped.length() > 0) {
                        item.setImageURLCropped(imagesArrayCropped.getString(0));
                        item.setImageURLOriginal(imagesArrayOriginal.getString(0));
                    }
                }
                return item;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }