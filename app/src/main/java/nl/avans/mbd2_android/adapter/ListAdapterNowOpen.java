package nl.avans.mbd2_android.adapter;

/**
 * Created by Sander on 7-4-2015.
 */

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nl.avans.mbd2_android.R;
import nl.avans.mbd2_android.models.DownloadImageTask;
import nl.avans.mbd2_android.models.Venue;

public class ListAdapterNowOpen extends ArrayAdapter<Venue> {

    private LayoutInflater inflater;

    public ListAdapterNowOpen(Activity activity, List<Venue> items) {
        super(activity, R.layout.listview_item_row, items);
        inflater = activity.getWindow().getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if( convertView == null ){
            //We must create a View:
            convertView = inflater.inflate(R.layout.listview_item_row, parent, false);
        }
        TextView header = (TextView) convertView.findViewById(R.id.row_itemTitle);
        header.setText(getItem(position).getTitle());
        TextView body = (TextView) convertView.findViewById(R.id.row_itemBody);
        body.setText(getItem(position).getBody());

        String url = getItem(position).getImageURLCropped();
        if (url != null) {
            ImageView iV = (ImageView) convertView.findViewById(R.id.row_itemImage);
            iV.setTag(url);
            new DownloadImageTask(iV).execute(url);
        }

        // Font Awesome
        Typeface font = Typeface.createFromAsset( convertView.getContext().getAssets(), "fontawesome-webfont.ttf" );

        Button button = (Button) convertView.findViewById( R.id.action_favorite );
        button.setTypeface(font);

        //Here we can do changes to the convertView, such as set a text on a TextView
        //or an image on an ImageView.
        return convertView;
    }
}