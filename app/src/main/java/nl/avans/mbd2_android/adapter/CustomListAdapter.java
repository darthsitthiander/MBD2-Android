package nl.avans.mbd2_android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import nl.avans.mbd2_android.R;
import nl.avans.mbd2_android.activities.MainActivity;
import nl.avans.mbd2_android.fragments.FragmentTabFavorites;
import nl.avans.mbd2_android.models.*;

/**
 * Created by Sander on 9-4-2015.
 */
public class CustomListAdapter extends BaseAdapter implements Filterable {

    private FragmentTabFavorites fragmentTabFavorites;
    private ArrayList<Venue> listData;
    private ArrayList<Venue> savedData;
    private LayoutInflater layoutInflater;
    private SharedPreference sharedPreference;
    private Context mContext;
    private boolean isLVFavoriteAdapter = false;

    private AdapterCallback mAdapterCallback;

    public CustomListAdapter(Context context) {
        this.mContext = context;
        this.listData = new ArrayList<Venue>();
        this.savedData = listData;
        this.sharedPreference = new SharedPreference();
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        // Font Awesome
        Typeface font;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listview_item_row, null);
            holder = new ViewHolder();
            holder.titleView = (TextView) convertView.findViewById(R.id.row_itemTitle);
            holder.bodyView = (TextView) convertView.findViewById(R.id.row_itemBody);
            holder.taglineView = (TextView) convertView.findViewById(R.id.row_itemTagline);
            holder.ratingView = (TextView) convertView.findViewById(R.id.row_itemRating);
            holder.imageView = (ImageView) convertView.findViewById(R.id.row_itemImage);
            holder.favoriteView = (ToggleButton) convertView.findViewById(R.id.action_favorite);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Venue listItem = listData.get(position);
        if(listItem != null){
            holder.titleView.setText(listItem.getTitle());
            holder.bodyView.setText(listItem.getBody());
            String tagline = listItem.getTagline();
            if(!tagline.equals("null")){
                holder.taglineView.setText(tagline);
            }
            Float rating = listItem.getRating();
            if(!rating.equals(0.0f)) {
                holder.ratingView.setText(convertView.getContext().getString(R.string.fa_star) + " " + rating);
            }
            holder.imageView.setTag(listItem.getImageURLCropped());
            font = Typeface.createFromAsset( convertView.getContext().getAssets(), "fontawesome-webfont.ttf" );
            String id = listItem.getVenueID();

            setOnClick(holder.favoriteView, position, Integer.valueOf(id));
            if(sharedPreference.inFavorites(convertView.getContext(), id)) {
                holder.favoriteView.setTextColor(Color.parseColor("#FFFF00"));
                holder.favoriteView.setChecked(true);
            }else {
                holder.favoriteView.setTextColor(Color.parseColor("#666666"));
                holder.favoriteView.setChecked(false);
            }
            holder.ratingView.setTypeface(font);
            holder.favoriteView.setTypeface(font);

            String url = listItem.getImageURLCropped();
            if (holder.imageView != null && url != null) {
                holder.imageView.setTag(url);
                new DownloadImageTask(holder.imageView).execute(url);
            }
        }

        return convertView;
    }

    public boolean hasFavorites() {
        return (this.listData.size() > 0);
    }

    public void setListData(ArrayList<Venue> listData) {
        this.listData = listData;
        this.savedData = listData;

    }

    public boolean isLVFavoriteAdapter() {
        return isLVFavoriteAdapter;
    }

    public void setLVFavoriteAdapter(boolean isLVFavoriteAdapter) {
        this.isLVFavoriteAdapter = isLVFavoriteAdapter;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                listData = (ArrayList<Venue>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<Venue> FilteredArrayNames = new ArrayList<Venue>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < savedData.size(); i++) {
                    Venue venue = savedData.get(i);
                    if (venue.getTitle().toLowerCase().startsWith(constraint.toString()))  {
                        FilteredArrayNames.add(venue);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;
                Log.e("VALUES", results.values.toString());

                return results;
            }
        };

        return filter;
    }

    static class ViewHolder {

        TextView titleView;
        TextView bodyView;
        TextView taglineView;
        TextView ratingView;
        ImageView imageView;
        ToggleButton favoriteView;
    }
    private void setOnClick(final Button btn, final int position, final int id){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Button clickedButton = (Button) view;
                boolean on = ((ToggleButton) view).isChecked();

                // Do whatever you want(str can be used here)
                Context context = view.getContext();
                int duration = Toast.LENGTH_SHORT;

                CharSequence text = null;
                if (on) {
                    // Set favorite
                    sharedPreference.addFavorite(context, String.valueOf(id));
                    clickedButton.setTextColor(Color.parseColor("#FFFF00"));
                    ((MainActivity) mAdapterCallback).addFavorite(listData.get(position));
                    text = "Toegevoegd aan favorieten";
                } else {
                    // Remove favorite
                    sharedPreference.removeFavorite(context, String.valueOf(id));
                    clickedButton.setTextColor(Color.parseColor("#FFFF00"));
                    if (isLVFavoriteAdapter()) {
                        removeListData(position);
                    } else {
                        ((MainActivity) mAdapterCallback).removeFavorite(listData.get(position));
                    }
                    notifyDataSetChanged();
                    text = "Verwijdert van favorieten";
                }

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

    public void removeListData(int position){
        listData.remove(position);
        this.savedData = this.listData;
        if(isLVFavoriteAdapter() && getCount() == 0){
            ((MainActivity) mAdapterCallback).refreshFavorites();
        }
    }

    public void setCallback(AdapterCallback callback){
        this.mAdapterCallback = callback;
        if(isLVFavoriteAdapter()){
            ((MainActivity) mAdapterCallback).setAdapterFavorites(this);
        }
    }

    public void addFavorite(Venue newFavorite) {

        this.listData.add(newFavorite);
        this.savedData = this.listData;
    }

    public void removeFavorite(Venue venue) {

        this.listData.remove(venue);
        this.savedData = this.listData;
    }

    public static interface AdapterCallback {
        void setAdapterFavorites(CustomListAdapter adapter);
        void refreshFavorites();
        void addFavorite(Venue favorite);
        void removeFavorite(Venue favorite);
    }
}