package nl.avans.mbd2_android.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import nl.avans.mbd2_android.R;
import nl.avans.mbd2_android.TabListener;
import nl.avans.mbd2_android.adapter.CustomListAdapter;
import nl.avans.mbd2_android.fragments.*;
import nl.avans.mbd2_android.models.AsyncListViewLoader;
import nl.avans.mbd2_android.models.AsyncListViewLoaderMultiple;
import nl.avans.mbd2_android.models.AsyncResponse;
import nl.avans.mbd2_android.models.SharedPreference;
import nl.avans.mbd2_android.models.Venue;

public class MainActivity extends Activity implements AsyncResponse, CustomListAdapter.AdapterCallback, SharedPreferences.OnSharedPreferenceChangeListener, FragmentTabFavorites.TabFavoritesListener {

    private SharedPreference sharedPreference;
    private SharedPreferences filters;
    private SharedPreferences.Editor editor;

    int iCurCheckPosition = 0;
    int iSelectedTab = 0;

    ActionBar.Tab tab1, tab2, tab3;
    FragmentTabFavorites fragmentFavorites;
    Fragment fragmentNowOpen;
    Fragment fragmentTop;

    CustomListAdapter adapterFavorites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreference = new SharedPreference();
        filters = PreferenceManager.getDefaultSharedPreferences(this.getBaseContext());
        editor = filters.edit();

        if (savedInstanceState != null) {
            // Restore the last tab position
            iSelectedTab = savedInstanceState.getInt("iSelectedTab");
        }

        setupFragmentFavorites();

        setupFragmentNowOpen();

        setupFragmentTop();

        setupTabs();
    }

    private void setupFragmentTop() {

        final ListView listViewTop = new ListView(this);
        CustomListAdapter clAdapter = new CustomListAdapter(getApplicationContext());
        clAdapter.setCallback((CustomListAdapter.AdapterCallback) this);
        listViewTop.setAdapter(clAdapter);

        AsyncListViewLoaderMultiple t = new AsyncListViewLoaderMultiple(this, this, listViewTop);
        // Filters
        String pref_type = filters.getString("pref_type", "");
        String pref_sortby = filters.getString("pref_sortby", "");
        String tags = "";
        String url = "https://api.eet.nu/venues?sort_by=";
        if(pref_sortby != null){
            switch(pref_sortby){
                case "rating":  url += "rating"; break;
                case "reviews":  url += "reviews"; break;
                default: url += "rating";
            }
        }
        if(pref_type != null && !pref_type.equals("all")){
            url += "&tags=" + pref_type;
        }
        t.execute(url);

        listViewTop.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {

                        iCurCheckPosition = position;

                        // Pass along the currently selected index assigned to the keyword index
                        Venue venue = (Venue) listViewTop.getAdapter().getItem(position);
                        Intent i = new Intent(fragmentTop.getActivity(), DetailsActivity.class);
                        i.putExtra("item", venue);
                        startActivity(i);
                    }
                }
        );
        fragmentTop = FragmentTabTop.newInstance(listViewTop);
    }

    private void setupFragmentNowOpen() {

        final ListView listViewNowOpen = new ListView(this);
        CustomListAdapter clAdapter = new CustomListAdapter(getApplicationContext());
        clAdapter.setCallback((CustomListAdapter.AdapterCallback) this);
        listViewNowOpen.setAdapter(clAdapter);

        AsyncListViewLoaderMultiple t = new AsyncListViewLoaderMultiple(this, this, listViewNowOpen);
        // Filters
        String pref_type = filters.getString("pref_type", "");
        String pref_sortby = filters.getString("pref_sortby", "");
        String url = "https://api.eet.nu/venues?sort_by=";
        if(pref_sortby != null){
            switch(pref_sortby){
                case "rating":  url += "rating"; break;
                case "reviews":  url += "reviews"; break;
                default: url += "rating";
            }
        }
        url += "&tags=now-open";
        if(pref_type != null && !pref_type.equals("all")){
            url += "," + pref_type;
        }
        System.out.println(url);
        t.execute(url);

        listViewNowOpen.setOnItemClickListener(
                new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {

                        iCurCheckPosition = position;

                        // Pass along the currently selected index assigned to the keyword index
                        Venue venue = (Venue) listViewNowOpen.getAdapter().getItem(position);
                        Intent i = new Intent(fragmentNowOpen.getActivity(), DetailsActivity.class);
                        i.putExtra("item", venue);
                        startActivity(i);
                    }
                }
        );
        fragmentNowOpen = FragmentTabNowOpen.newInstance(listViewNowOpen);
    }

    private void setupFragmentFavorites() {
        List<String> initialFavorites = sharedPreference.getFavorites(this);

        final ListView listViewFavorites = new ListView(this);
        CustomListAdapter clAdapter = new CustomListAdapter(getApplicationContext());
        clAdapter.setLVFavoriteAdapter(true);
        clAdapter.setCallback((CustomListAdapter.AdapterCallback) this);
        listViewFavorites.setAdapter(clAdapter);


        fragmentFavorites = FragmentTabFavorites.newInstance(listViewFavorites);

        AsyncListViewLoader t = new AsyncListViewLoader(this, this, listViewFavorites);
        if(initialFavorites != null && initialFavorites.size() > 0){
            t.execute(initialFavorites);
        }
        listViewFavorites.setOnItemClickListener(
                new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {

                        // Pass along the currently selected index assigned to the keyword index
                        Venue venue = (Venue) listViewFavorites.getAdapter().getItem(position);
                        Intent i = new Intent(fragmentFavorites.getActivity(), DetailsActivity.class);
                        i.putExtra("item", venue);
                        startActivity(i);
                    }
                }
        );
    }

    private void setupTabs() {
        // Asking for the default ActionBar element that our platform supports.
        ActionBar actionBar = getActionBar();

        // ActionBar icon
        actionBar.setDisplayShowHomeEnabled(false);

        // ActionBar title
        actionBar.setDisplayShowTitleEnabled(true);

        // Creating ActionBar tabs.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Setting custom tab icons.
        tab1 = actionBar.newTab().setText("Favoriten");
        tab2 = actionBar.newTab().setText("Nu open");
        tab3 = actionBar.newTab().setText("Top");

        // Setting tab listeners.
        tab1.setTabListener(new TabListener(fragmentFavorites));
        tab2.setTabListener(new TabListener(fragmentNowOpen));
        tab3.setTabListener(new TabListener(fragmentTop));

        // Adding tabs to the ActionBar.
        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        actionBar.addTab(tab3);

        actionBar.setSelectedNavigationItem(iSelectedTab);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        Menu menu_sort = (Menu) findViewById(R.id.group_sorting);
        menu.add("sdaff");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.menuFilterName:
            case R.id.menuSortKitchen:
            case R.id.menuSortLocation:
            case R.id.menuSortRating:
            case R.id.menuSortReviews:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                reloadActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void processFinish(String output) {

    }

    @Override
    public void setAdapterFavorites(CustomListAdapter adapter) {

        this.adapterFavorites = adapter;
    }

    @Override
    public void refreshFavorites() {
        fragmentFavorites.refresh();
    }

    @Override
    public void addFavorite(Venue newFavorite){
        adapterFavorites.addFavorite(newFavorite);
    }

    @Override
    public void removeFavorite(Venue venue) {
        adapterFavorites.removeFavorite(venue);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the current tab position
        super.onSaveInstanceState(outState);
        outState.putInt("iSelectedTab", getActionBar().getSelectedNavigationIndex());
    }

    public void reloadActivity(){
        finish();
        startActivity(getIntent());
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        filters.registerOnSharedPreferenceChangeListener(this);

        if(filters.getBoolean("filters_changed", false)){
            editor.putBoolean("filters_changed", false);
            editor.commit();

            reloadActivity();
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(!key.equals("filters_changed")){
            editor.putBoolean("filters_changed", true);
            editor.commit();
        }
    }

    @Override
    public List<String> getFavorites() {
        return sharedPreference.getFavorites(this);
    }
}
